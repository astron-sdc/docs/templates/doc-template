DOCHANDLE=SDC-example
export TEXMFHOME ?= astron-texmf/texmf

$(DOCHANDLE).pdf: main.tex meta.tex changes.tex
	xelatex -jobname=$(DOCHANDLE) main.tex
	makeglossaries $(DOCHANDLE)
	biber $(DOCHANDLE)
	xelatex -jobname=$(DOCHANDLE) main.tex
	xelatex -jobname=$(DOCHANDLE) main.tex

include astron-texmf/make/vcs-meta.make
include astron-texmf/make/changes.make
